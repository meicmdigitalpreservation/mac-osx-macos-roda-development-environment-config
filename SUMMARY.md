# Summary

* [Summary](README.md)
* [Requisites](requisites.md)
* [Setting Up RODA in IntelliJ](setting-up-roda-in-intellij.md)
* [Running RODA from IntelliJ](running-roda-from-intellij.md)
* [Setting Up GWT SDK in IntelliJ](setting-up-gwt-sdk-in-intellij.md)

