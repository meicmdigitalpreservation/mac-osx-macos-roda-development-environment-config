# Requisites

This guide focuses on the configuration of the development environment for RODA Repository under a Mac OSX / MacOS environment. Therefore, a computer running any version of Mac OSX or MacOS operating system is a base requirement for the configuration process described by this guide.

The requisites for this guide are:

* A copy of **RODA Repository code**. The latest source code can be downloaded in [https://github.com/keeps/roda](https://github.com/keeps/roda).

* **JetBrains IntelliJ IDEA** IDE. In this guide we will use the **2017 Ultimate** edition. The IDE can be downloaded in [https://www.jetbrains.com/idea/download/\#section=mac](https://www.jetbrains.com/idea/download/#section=mac).

* **Google Web Toolkit SDK** \(Software Development Kit\). RODA relies in this technology to implement its web application architecture. Although RODA ships with **GWT version 2.7.0**, in this guide we will also demonstrate the configuration of **version 2.8.0** which can be downloaded in [http://www.gwtproject.org/versions.html](http://www.gwtproject.org/versions.html).

Additionally, you can use a Git Client application to checkout RODA source code from the repository. If you wish to do so, you can use Atlassian Source Tree which can be downloaded in [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/).

All the software used in this guide is available for free or under an open source license except for the IntelliJ IDEA IDE. JetBrains offers a free and open source Community edition of the IDE. However, since the Community version does not support the development of Google Web Toolkit applications we used the Ultimate edition.

