# Mac OSX / MacOS Development Environment Configuration Guide for RODA Repository

This document is a step by step guide for the configuration of a development environment for the RODA Repository under a Mac OSX / MacOS operating system.

RODA, short for Repository of Authentic Digital Objects, is a digital repository software projected with digital preservation in mind, implementing all the functional units proposed by the OAIS reference model. The repository is capable of ingesting, managing and providing access to several different types of digital content produced either by companies, public entities or any other institution or individual. RODA is based in open source technologies and supported by existing standards like Open Archival Information System \(OAIS\), Metadata Encoding and Transmission Standard \(METS\), Encoded Archival Description \(EAD\), Dublin Core \(DC\) and PREservation Metadata Information Standard \(PREMIS\).

For further information, please visit [http://www.roda-community.org](http://www.roda-community.org).

## Author {#markdown-header-author}

**André Rosa**

* [https://bitbucket.org/candrelsrosa](https://bitbucket.org/candrelsrosa)
* [https://github.com/andreros/](https://github.com/andreros/)
* [https://facebook.com/candrelsrosa](https://facebook.com/candrelsrosa)
* [https://twitter.com/candrelsrosa](https://twitter.com/candrelsrosa)

## License {#markdown-header-license}

This guide is released under the [Creative Commons Attribution 4.0 International \(CC BY 4.0\)](https://creativecommons.org/licenses/by/4.0/deed.en) license.

