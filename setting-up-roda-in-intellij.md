# Setting Up RODA in IntelliJ

To complete this guide we assume you have a computer with a Mac OSX / MacOS operating system and the IntelliJ IDEA IDE already installed.

To initiate RODA project setup, please, download RODA latest source code from [https://github.com/keeps/roda](https://github.com/keeps/roda). [Figure 1](/assets/01 - Download RODA Source Code.png) illustrates RODA GitHub page. To download the source code, please, click the "Clone or download" button, identified in the image with the number 1, and choose the "Download ZIP" option. The code should be downloaded to your computer.

**Figure 1 - RODA GitHub Page**

![](/assets/01 - Download RODA Source Code.png)

Extract the ZIP file contents to a folder in your computer. Please, bear in mind the folder you choose will be your project working folder. We suggest placing RODA project folder inside the "/Users/&lt;username&gt;/" folder, as illustrated by [Figure 2](/assets/02 - Roda-master project folder.png).

**Figure 2 - RODA Project Folder**

![](/assets/02 - Roda-master project folder.png)

Open IntelliJ IDEA IDE and from the initial screen, illustrated by [Figure 3](/assets/03 - IntelliJ Open Project.png), choose the "Open" option. Next, select RODA project folder location and click the "Open" button.

**Figure 3 - IntelliJ IDEA IDE - Open Project Screen**

![](/assets/03 - IntelliJ Open Project.png)

The project will be detected as a Maven project by the IDE and all needed dependencies will automatically be indexed and downloaded. Since RODA is not a small project, the dependencies download may take some time, depending on the your internet connection speed. [Figure 4](/assets/04 - IntelliJ Resolving Dependencies.png) shows IntelliJ IDE "Background Tasks" window resolving the project dependencies automatically.

**Figure 4 - IntelliJ IDEA IDE - Resolving Project Dependencies**

![](/assets/04 - IntelliJ Resolving Dependencies.png)

Once all project dependencies are downloaded and indexed, the IntelliJ IDEA IDE will detect the presence of the Google Web Toolkit framework in the project. You will be prompted to configure the frameworks detected. To access the configuration window and setup the project frameworks, please click the "Configure" link, in blue, as shown by [Figure 5](/assets/05 - IntelliJ GWT Framework Detected.png).

**Figure 5 - IntelliJ IDEA IDE - Google Web Toolkit Framework Detected**

![](/assets/05 - IntelliJ GWT Framework Detected.png)

To automatically configure the detected frameworks, select the checkboxes behind each framework presented and click the "OK" button. [Figure 6](/assets/06 - IntelliJ GWT Framework Setup.png) presents the frameworks detected by IntelliJ IDEA IDE for the RODA project.

**Figure 6 - IntelliJ IDEA IDE - Google Web Toolkit Framework Configuration**

![](/assets/06 - IntelliJ GWT Framework Setup.png)

After the frameworks configuration, the project is finally loaded inside IntelliJ. If the process went well, you should be seeing a RODA project file tree like the one shown in [Figure 7](/assets/07 - RODA Project Tree in IntelliJ.png).

**Figure 7 - RODA Project Tree in IntelliJ**

![](/assets/07 - RODA Project Tree in IntelliJ.png)

This concludes RODA project configuration inside the IntelliJ IDEA IDE. Proceed to [Running RODA from IntelliJ](/running-roda-from-intellij.md) Chapter.

