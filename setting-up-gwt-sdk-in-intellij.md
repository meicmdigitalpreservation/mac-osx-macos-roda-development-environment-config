# Setting up Google Web Toolkit Software Development Kit in IntelliJ

Although RODA ships with **Google Web Toolkit version 2.7.0**, in this guide we will also demonstrate the configuration of another version in case you need to. For this purpose we will use **version 2.8.0**.

First, we will start by downloading version 2.8.0 from the Google Web Toolkit website. [Figure 12](/assets/12 - Download GWT version 2.8.0.png) illustrates GTW project download page.

**Figure 12 - Download Google Web Toolkit version 2.8.0**

![](/assets/12 - Download GWT version 2.8.0.png)

Once we have downloaded the library "gwt-2.8.0.zip" file, we must extract its contents to a folder. We suggest placing the GWT library folder inside the "/Users/&lt;username&gt;/" folder, as previously done with RODA project folder, for reference.

Next, we need to configure RODA project to point to the new GWT library files. To do so, from the IDE "File" menu, we choose the option "Project Structure..." as shown by [Figure 13](/assets/13 - IntelliJ Open Project Structure.png).

**Figure 13 - IntelliJ IDEA IDE - Open Project Structure**

![](/assets/13 - IntelliJ Open Project Structure.png)

The project structure window will open. From the options presented on the left, we choose "Facets". In the window center panel we will see the frameworks used by the RODA project. By clicking on the "GWT" node in the tree we open the framework configuration in a panel on the right hand side of the window.

To configure the new GWT library files, we click the icon "..." and point to the folder where we extracted the files. Then, we click the "Apply" button followed by the "OK" button.

**Figure 14 - IntelliJ IDEA IDE - Project Structure Window**

![](/assets/14 - IntelliJ Configure Project Structure.png)

This concludes RODA project GWT alternative version configuration and this guide.

