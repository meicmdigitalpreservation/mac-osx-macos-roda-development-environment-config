# Running RODA from IntelliJ

Now that you completed successfully the [Setting Up RODA in IntelliJ](/setting-up-roda-in-intellij.md) Chapter, you can run RODA project from the IntelliJ IDEA IDE. To run the project, first you need to create a new configuration. In the right upper corner of the IDE window, click the dropdown box with the label "Unnamed". From the presented options now choose "Edit Configurations...". The "Run/Debug Configurations" window should open. In the left upper corner of the window click the "+" icon and choose the option "GWT Configuration" from the list, as shown in [Figure 8](/assets/08 - Add New Configuration.png).

**Figure 8 - RODA Project - Add New Configuration**

![](/assets/08 - Add New Configuration.png)

IntelliJ will then create a new blank GTW Configuration and present it in the right side of the "Run/Debug Configurations" window. You should change the configuration name from "Unnamed" to something more meaningful like, for example, "RODA". The chosen name will be presented to you in the configurations dropdown box in the right upper corner of the IDE window.

The fields which need to be configured are:

* **Module** - Choose "**roda-wui**".
* **Use Super Dev Mode** - If it is unchecked, check this box.
* **VM Options** - Change the pre-configured value from "-Xmx512m" to "**-Xmx2048m**". This will increase the total memory available when running RODA server.
* **JRE** - If this setting is not defined, choose a JRE SDK from the ones installed in your system. It is recommended that you use **JRE 1.8** although JRE 1.7 will work as well.
* **Before launch: Build, Activate tool window** - Make sure you have the "**Build**" option in the list, otherwise the server will not start.

Once you complete the configuration, click the "Apply" button followed by the "OK" button. [Figure 9](/assets/09 - IntelliJ Run Debug Configurations Window.png) illustrates the correct configuration for the project.

**Figure 9 - RODA Project - Run/Debug Configurations Window**

![](/assets/09 - IntelliJ Run Debug Configurations Window.png)

Now that the new configuration is in place, click the green "Play" icon in the right upper corner of the IntelliJ IDEA IDE to run the project. The IDE will open the "Run" panel in the lower part of the window and bootstrap RODA server. After the server is up, the IDE will also launch the front-end application and open it in your default browser window. [Figure 10](/assets/10 - Run RODA Project.png) illustrates the RODA server boostrap in the IDE "Run" panel.

**Figure 10 - Run RODA Project**

![](/assets/10 - Run RODA Project.png)

If the RODA instance started successfully, you should now be seeing the running instance in your browser window, as shown in [Figure 11](/assets/11 - RODA Repository.png).

**Figure 11 - RODA Repository Instance Running**

![](/assets/11 - RODA Repository.png)

This concludes RODA project running inside the IntelliJ IDEA IDE. Additionally, you can read the [Setting Up GWT SDK in IntelliJ](/setting-up-gwt-sdk-in-intellij.md) Chapter.

